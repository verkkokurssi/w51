
import './App.css';

function App() {

  const onClick = (e) => {
    e.preventDefault();
    
    const name = e.target.name.value;
    const age = e.target.age.value;

    // Check if name and age are not empty
    if(name === '' || age === '') {
      alert('Please fill all the fields');
      return;
    }

    // Check if name has first letter capitalized
    if(name[0] !== name[0].toUpperCase()) {
      alert('Name should start with a capital letter');
      return;
    }

    // Check if age is integer between 0-125
    if(!Number.isInteger(+age) || +age < 0 || +age > 125) {
      alert('Age should be an integer between 0-125');
      return;
    }

    // Create JSON object
    let person = {};
    person.name = name;
    person.age = +age;

    const valueText = document.getElementById("valuetext");
    valueText.innerHTML = JSON.stringify(person);
  }

  return (
    <div className="App">
      <form onSubmit={onClick}>
        <input type="text" id="name" placeholder="Name" />
        <input type="number" id="age" placeholder="Age" />
        <button type="submit">Submit</button>
      </form>
      <p id="valuetext"></p>
    </div>
  );
}

export default App;
